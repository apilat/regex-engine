# regex-engine

This repository is my attempt at implementing a simple regular expression engine.

## Features

- [x] Quantifiers - `*`, `?`, `+`, `{n,m}`
- [x] Groups and alternations - `(.*)`, `|`
    - [ ] Capturing groups
- [x] Character sets - `[abc]`, `[0-9]`
- [ ] Escapes - `\\ `, `\n`, `\r`
- [x] NFA-based matcher which does not suffer from catastrophic backtracing
