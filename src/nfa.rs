use super::parser::{Element, Quantifier, Value, ValueRange};
use std::collections::HashSet;

/// Internal representation of the non-finite automatom that matches a regex.
#[derive(Debug, Clone)]
pub struct Nfa {
    states: Vec<NfaState>,
}

/// A single state in an NFA. Only meaningful with the NFA that it was created from, but this is
/// not statically enforced.
#[derive(Debug, Clone)]
struct NfaState {
    /// Connection to other states in the NFA. Consists of the index of the new state and the
    /// character that is consumed.
    transitions: Vec<(usize, TransitionType)>,
    is_final: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum TransitionType {
    Char(char),
    Range(char, char), // inclusive
    Wildcard,
    Epsilon,
}

impl Nfa {
    pub fn new(element: &Element) -> Self {
        let mut nfa = Nfa { states: Vec::new() };
        let entry = nfa.create_new_state();
        let (start, finish) = nfa.add_element(element);

        nfa.create_link(entry, start);
        nfa.states[finish].is_final = true;
        nfa
    }

    fn create_new_state(&mut self) -> usize {
        self.states.push(NfaState {
            transitions: Vec::new(),
            is_final: false,
        });
        self.states.len() - 1
    }

    fn create_transition(
        &mut self,
        initial_state: usize,
        final_state: usize,
        requirement: TransitionType,
    ) {
        self.states[initial_state]
            .transitions
            .push((final_state, requirement));
    }

    fn create_link(&mut self, initial_state: usize, final_state: usize) {
        self.create_transition(initial_state, final_state, TransitionType::Epsilon)
    }

    fn add_element(&mut self, element: &Element) -> (usize, usize) {
        if let Quantifier::Range { min, max } = element.quantifier {
            // TODO Remove unnecessary allocations of Vec and Values
            let mut equiv = Vec::new();
            for _ in 0..min {
                equiv.push(Element {
                    value: element.value.clone(),
                    quantifier: Quantifier::One,
                });
            }
            for _ in 0..max - min {
                equiv.push(Element {
                    value: element.value.clone(),
                    quantifier: Quantifier::Optional,
                });
            }
            self.add_value(&Value::Group(equiv))
        } else {
            let (start, end) = self.add_value(&element.value);
            self.add_quantifier(&element.quantifier, start, end)
        }
    }

    fn add_value(&mut self, value: &Value) -> (usize, usize) {
        match value {
            Value::One(c) => {
                let (start, end) = (self.create_new_state(), self.create_new_state());
                self.create_transition(start, end, TransitionType::Char(*c));
                (start, end)
            }

            Value::Wildcard => {
                let (start, end) = (self.create_new_state(), self.create_new_state());
                self.create_transition(start, end, TransitionType::Wildcard);
                (start, end)
            }

            Value::Group(inner) => {
                if let Some(first) = inner.first() {
                    let (start, mut end) = self.add_element(first);
                    for elem in &inner[1..] {
                        let (int_start, int_end) = self.add_element(elem);
                        self.create_link(end, int_start);
                        end = int_end;
                    }
                    (start, end)
                } else {
                    let empty = self.create_new_state();
                    (empty, empty)
                }
            }

            Value::Or(left, right) => {
                let (start, end) = (self.create_new_state(), self.create_new_state());
                let (lstart, lend) = self.add_element(left);
                let (rstart, rend) = self.add_element(right);
                self.create_link(start, lstart);
                self.create_link(start, rstart);
                self.create_link(lend, end);
                self.create_link(rend, end);
                (start, end)
            }

            Value::Set(set) => {
                let (start, end) = (self.create_new_state(), self.create_new_state());
                for opt in set {
                    match opt {
                        ValueRange::Single(c) => {
                            self.create_transition(start, end, TransitionType::Char(*c))
                        }
                        ValueRange::Range(a, b) => {
                            self.create_transition(start, end, TransitionType::Range(*a, *b))
                        }
                    }
                }
                (start, end)
            }
        }
    }

    fn add_quantifier(
        &mut self,
        quantifier: &Quantifier,
        start: usize,
        mut end: usize,
    ) -> (usize, usize) {
        match quantifier {
            Quantifier::One => (),
            Quantifier::Optional => self.create_link(start, end),
            Quantifier::Many => self.create_link(end, start),
            Quantifier::ZeroOrMany => {
                self.create_link(end, start);
                end = start;
            }
            Quantifier::Range { .. } => {
                unreachable!("Range should be transformed before we get here")
            }
        }
        (start, end)
    }

    pub fn matches(&self, s: &str) -> bool {
        let mut states = vec![0];

        for c in s.chars() {
            let mut visited = HashSet::new();
            let mut new_states = Vec::new();

            while let Some(state) = states.pop() {
                if !visited.insert(state) {
                    // State already visited
                    continue;
                }

                for (next_state, transition) in self.states[state].transitions.iter() {
                    use TransitionType::*;
                    match transition {
                        Char(x) => {
                            if *x == c {
                                new_states.push(*next_state);
                            }
                        }
                        Range(x, y) => {
                            if *x <= c && c <= *y {
                                new_states.push(*next_state);
                            }
                        }
                        Wildcard => new_states.push(*next_state),
                        // If nothing is consumed, add it to current states and keep iterating.
                        Epsilon => states.push(*next_state),
                    }
                }
            }

            states = new_states;
        }

        // We need to traverse to make sure we account for any final states reachable through
        // Epsilon.
        while let Some(state) = states.pop() {
            let state = &self.states[state];
            if state.is_final {
                return true;
            }

            for (next_state, transition) in state.transitions.iter() {
                use TransitionType::*;
                if let Epsilon = transition {
                    states.push(*next_state);
                }
            }
        }
        false
    }
}

// There are deliberately no tests for the construction of Nfa internal since the implementation
// might be optimized in the future. Tests should instead verify accuracy or performance.
#[cfg(test)]
mod tests {
    use super::Nfa;
    use crate::parser::{ElementBuilder as B, ValueRange as R};

    #[test]
    fn simple() {
        let nfa = Nfa::new(&B::group(vec![]).one());
        assert!(nfa.matches(""));
        assert!(!nfa.matches("a"));

        let nfa = Nfa::new(&B::group(vec![B::char('a').one(), B::char('b').one()]).one());
        assert!(nfa.matches("ab"));
        assert!(!nfa.matches(""));
        assert!(!nfa.matches("a"));
        assert!(!nfa.matches("b"));
        assert!(!nfa.matches("aab"));
        assert!(!nfa.matches("abb"));

        let nfa = Nfa::new(
            &B::group(vec![
                B::char('c').one(),
                B::wildcard().one(),
                B::char('d').one(),
            ])
            .one(),
        );
        assert!(!nfa.matches(""));
        assert!(nfa.matches("ccd"));
        assert!(nfa.matches("cxd"));
        assert!(!nfa.matches("cd"));
    }

    #[test]
    fn optional() {
        let nfa = Nfa::new(
            &B::group(vec![
                B::char('b').optional(),
                B::char('c').one(),
                B::char('d').optional(),
            ])
            .one(),
        );
        assert!(!nfa.matches(""));
        assert!(nfa.matches("c"));
        assert!(!nfa.matches("d"));
        assert!(nfa.matches("cd"));
        assert!(!nfa.matches("cdd"));
        assert!(nfa.matches("bc"));
        assert!(nfa.matches("bcd"));

        let nfa = Nfa::new(&B::char('a').optional());
        assert!(nfa.matches(""));
        assert!(nfa.matches("a"));
        assert!(!nfa.matches("aa"));
    }

    #[test]
    fn greedy_matching() {
        // a*b?
        let nfa =
            Nfa::new(&B::group(vec![B::char('a').zero_or_many(), B::char('b').optional()]).one());
        assert!(nfa.matches(""));
        assert!(nfa.matches("aaaaaaaaaaa"));
        assert!(nfa.matches("b"));
        assert!(!nfa.matches("aabb"));
        assert!(!nfa.matches("aaaba"));

        // b+c?b
        let nfa = Nfa::new(
            &B::group(vec![
                B::char('b').many(),
                B::char('c').optional(),
                B::char('b').one(),
            ])
            .one(),
        );
        assert!(!nfa.matches(""));
        assert!(!nfa.matches("b"));
        assert!(nfa.matches("bbbb"));
        assert!(nfa.matches("bbbbbcb"));
        assert!(!nfa.matches("bbcbb"));
        assert!(!nfa.matches("cb"));
    }

    #[test]
    fn groups() {
        // a?(ab)*
        let nfa = Nfa::new(
            &B::group(vec![
                B::char('a').optional(),
                B::group(vec![B::char('a').one(), B::char('b').one()]).zero_or_many(),
            ])
            .one(),
        );
        assert!(nfa.matches(""));
        assert!(nfa.matches("ababab"));
        assert!(nfa.matches("aabab"));
        assert!(!nfa.matches("bababab"));
        assert!(!nfa.matches("abb"));

        // (a.)+(ba)?
        let nfa = Nfa::new(
            &B::group(vec![
                B::group(vec![B::char('a').one(), B::wildcard().one()]).many(),
                B::group(vec![B::char('b').one(), B::char('a').one()]).optional(),
            ])
            .one(),
        );
        assert!(!nfa.matches(""));
        assert!(nfa.matches("ax"));
        assert!(!nfa.matches("a"));
        assert!(!nfa.matches("aba"));
        assert!(nfa.matches("axayab"));
        assert!(nfa.matches("axayba"));
        assert!(nfa.matches("axayaz"));
        assert!(!nfa.matches("axayza"));
    }

    #[test]
    fn alterations() {
        // a+|b*|c
        let nfa = Nfa::new(
            &B::or(
                B::or(B::char('a').many(), B::char('b').zero_or_many()).one(),
                B::char('c').one(),
            )
            .one(),
        );
        assert!(nfa.matches(""));
        assert!(nfa.matches("a"));
        assert!(nfa.matches("aaaaa"));
        assert!(!nfa.matches("ba"));
        assert!(!nfa.matches("ab"));
        assert!(nfa.matches("c"));
        assert!(!nfa.matches("cc"));
        assert!(!nfa.matches("bc"));
        assert!(!nfa.matches("cb"));
    }

    #[test]
    fn range() {
        let nfa = Nfa::new(&B::char('a').range(2, 5));
        assert!(!nfa.matches(""));
        assert!(!nfa.matches("a"));
        assert!(nfa.matches("aa"));
        assert!(nfa.matches("aaa"));
        assert!(nfa.matches("aaaa"));
        assert!(nfa.matches("aaaaa"));
        assert!(!nfa.matches("aaaaaa"));

        let nfa = Nfa::new(&B::wildcard().range(0, 0));
        assert!(nfa.matches(""));
        assert!(!nfa.matches("x"));
    }

    #[test]
    fn set() {
        let nfa = Nfa::new(&B::set(vec![R::Single('a'), R::Range('A', 'Z')]).many());
        assert!(!nfa.matches(""));
        assert!(nfa.matches("a"));
        assert!(!nfa.matches("b"));
        assert!(nfa.matches("A"));
        assert!(nfa.matches("Z"));
        assert!(nfa.matches("aaZA"));
        assert!(nfa.matches("ABCDEFG"));
        assert!(!nfa.matches("abcdefg"));
    }

    #[test]
    fn complex() {
        // .+(\..*)@.+
        let nfa = Nfa::new(
            &B::group(vec![
                B::wildcard().many(),
                B::group(vec![B::char('.').one(), B::wildcard().zero_or_many()]).one(),
                B::char('@').one(),
                B::wildcard().many(),
            ])
            .one(),
        );
        assert!(nfa.matches("admin.@example.org"));
        assert!(!nfa.matches("admin@example.org"));
        assert!(!nfa.matches("admin.@"));
        assert!(!nfa.matches("@example.org"));
        assert!(nfa.matches("admin.@example.org"));
        assert!(nfa.matches("admin.work@example.org"));
        assert!(!nfa.matches(".work@example.org"));

        // ((a|b)(c|d))+
        let nfa = Nfa::new(
            &B::group(vec![
                B::or(B::char('a').one(), B::char('b').one()).one(),
                B::or(B::char('c').one(), B::char('d').one()).one(),
            ])
            .many(),
        );
        assert!(!nfa.matches(""));
        assert!(nfa.matches("ac"));
        assert!(nfa.matches("bcbd"));
        assert!(nfa.matches("acadacbd"));
        assert!(!nfa.matches("bccd"));
        assert!(!nfa.matches("adda"));
        assert!(!nfa.matches("acbdaa"));

        // 0x([0-9a-f]+|[0-9A-F]+)|[0-9]+
        let nfa = Nfa::new(
            &B::or(
                B::group(vec![
                    B::char('0').one(),
                    B::char('x').one(),
                    B::or(
                        B::set(vec![R::Range('0', '9'), R::Range('a', 'f')]).many(),
                        B::set(vec![R::Range('0', '9'), R::Range('A', 'F')]).many(),
                    )
                    .one(),
                ])
                .one(),
                B::set(vec![R::Range('0', '9')]).many(),
            )
            .one(),
        );
        assert!(!nfa.matches(""));
        assert!(nfa.matches("0123"));
        assert!(nfa.matches("0x0123"));
        assert!(nfa.matches("0xab"));
        assert!(nfa.matches("0xAB"));
        assert!(!nfa.matches("0xaB"));
        assert!(!nfa.matches("ab"));
    }
}
