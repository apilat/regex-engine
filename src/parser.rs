use std::collections::HashSet;
use std::iter::FromIterator;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Element {
    pub(crate) value: Value,
    pub(crate) quantifier: Quantifier,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[non_exhaustive]
pub enum Value {
    Or(Box<Element>, Box<Element>),
    Group(Vec<Element>),
    Wildcard,
    One(char),
    Set(HashSet<ValueRange>),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ValueRange {
    Single(char),
    Range(char, char),
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[non_exhaustive]
pub enum Quantifier {
    One,
    Optional,
    Many,
    ZeroOrMany,
    Range { min: u32, max: u32 },
}

/// Parsing error caused by an incorrect pattern.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ParseError {
    /// The first character of the pattern is a quantifier.
    InitialQuantifier,
    /// Multiple quantifiers were applied to a single character at the given index.
    ///
    /// Note: This is supported in some regex engines as lazy or eager matching but this module
    /// does not currently support it.
    MultipleQuantifier(usize),
    /// An opened group is never closed (started on the given index).
    UnfinishedGroup(usize),
    /// A group is closed that was never opened.
    UnexpectedGroupEnd(usize),
    /// Integer inside quantifier could not be parsed.
    IntegerParseError(std::num::ParseIntError),
    /// In an range quantifier `{n,m}`, `n > m`
    InvertedRange(u32, u32),
    /// In a set `[a-b]`, 'a > b'
    InvertedSetRange(char, char),
}

/// Utility function for calling `parse()` on the `char`acters of a `str`.
pub fn parse_str(pattern: &str) -> Result<Element, ParseError> {
    parse(&mut pattern.chars().enumerate())
}

/// Parse the chars into a valid regex.
/// Takes in an iterator of `(index, char)` in order to enable easier re-use.
pub fn parse(pattern: &mut impl Iterator<Item = (usize, char)>) -> Result<Element, ParseError> {
    #[derive(Debug)]
    struct Group {
        start_index: usize,
        elements: Vec<Element>,
        alteration_lhs: Option<Element>,
    }
    impl Group {
        fn new(start_index: usize) -> Self {
            Self {
                start_index,
                elements: Vec::new(),
                alteration_lhs: None,
            }
        }

        // Condenses the current elements into a single, replacing them with an empty vector.
        // This allows easier code-reuse with add_alteration although it would make more sense if
        // this function consumed the group.
        fn take(&mut self) -> Element {
            let mut elements = std::mem::replace(&mut self.elements, Vec::new());
            let one_element = if elements.len() == 1 {
                elements.remove(0)
            } else {
                ElementBuilder::group(elements).one()
            };

            if let Some(lhs) = self.alteration_lhs.take() {
                ElementBuilder::or(lhs, one_element).one()
            } else {
                one_element
            }
        }
    }

    #[derive(Debug)]
    struct Stack(Vec<Group>);
    impl Stack {
        fn new() -> Self {
            Self(vec![Group::new(0)])
        }

        fn push(&mut self, elem: Element) {
            // There will always be at least one group as long as the error from pop_group is
            // handled.
            self.0.last_mut().unwrap().elements.push(elem);
        }

        fn set_last_quantifier(
            &mut self,
            quantifier: Quantifier,
            i: usize,
        ) -> Result<(), ParseError> {
            let last = self
                .0
                .last_mut()
                .unwrap()
                .elements
                .last_mut()
                .ok_or(ParseError::InitialQuantifier)?;
            if last.quantifier == Quantifier::One {
                last.quantifier = quantifier;
                Ok(())
            } else {
                Err(ParseError::MultipleQuantifier(i))
            }
        }

        fn push_group(&mut self, i: usize) {
            self.0.push(Group::new(i));
        }

        fn pop_group(&mut self, i: usize) -> Result<Element, ParseError> {
            if self.0.len() == 1 {
                // The only group left is the default, which was not explicitly opened, so it
                // cannot be closed by the user.
                Err(ParseError::UnexpectedGroupEnd(i))
            } else {
                // The length can only be 0 if a previous error from this function was ignored.
                Ok(self.0.pop().unwrap().take())
            }
        }

        fn add_alteration(&mut self) {
            let mut group = self.0.last_mut().unwrap();
            group.alteration_lhs = Some(group.take());
        }

        fn take(mut self) -> Result<Element, ParseError> {
            let mut iter = self.0.drain(..);
            let elem = iter.next().unwrap().take();
            if let Some(Group { start_index, .. }) = iter.next() {
                Err(ParseError::UnfinishedGroup(start_index))
            } else {
                Ok(elem)
            }
        }
    }

    let mut elements = Stack::new();

    while let Some((i, c)) = pattern.next() {
        match c {
            '.' => elements.push(ElementBuilder::wildcard().one()),

            '+' => elements.set_last_quantifier(Quantifier::Many, i)?,
            '*' => elements.set_last_quantifier(Quantifier::ZeroOrMany, i)?,
            '?' => elements.set_last_quantifier(Quantifier::Optional, i)?,

            '(' => elements.push_group(i),
            ')' => {
                let group = elements.pop_group(i)?;
                elements.push(group);
            }

            '{' => {
                let start_index = i;
                let mut inner = String::new();
                loop {
                    match pattern.next() {
                        None => return Err(ParseError::UnfinishedGroup(start_index)),
                        Some((_, '}')) => break,
                        Some((_, c)) => inner.push(c),
                    }
                }

                let mut iter = inner.split(',');
                let min = iter
                    .next()
                    .ok_or(ParseError::UnexpectedGroupEnd(i + 1))?
                    .parse()
                    .map_err(ParseError::IntegerParseError)?;
                match iter
                    .next()
                    .map(|n| n.parse().map_err(ParseError::IntegerParseError))
                {
                    None => elements
                        .set_last_quantifier(Quantifier::Range { min, max: min }, start_index)?,
                    Some(Err(e)) => return Err(e),
                    Some(Ok(max)) => {
                        if min <= max {
                            elements
                                .set_last_quantifier(Quantifier::Range { min, max }, start_index)?;
                        } else {
                            return Err(ParseError::InvertedRange(min, max));
                        }
                    }
                }
            }
            '}' => return Err(ParseError::UnexpectedGroupEnd(i)),

            '[' => {
                let start_index = i;
                let mut set = Vec::new();
                let mut range_lhs = None;

                loop {
                    match pattern.next() {
                        None => return Err(ParseError::UnfinishedGroup(start_index)),
                        Some((_, ']')) => break,
                        Some((_, c)) if range_lhs.is_some() => {
                            // [!--]
                            let left = range_lhs.take().unwrap();
                            if left <= c {
                                set.push(ValueRange::Range(left, c));
                            } else {
                                return Err(ParseError::InvertedSetRange(left, c));
                            }
                        }
                        Some((_, '-')) => match set.pop() {
                            Some(ValueRange::Single(c)) => range_lhs = Some(c),
                            Some(r @ ValueRange::Range { .. }) => {
                                // [a-b-]
                                set.push(r);
                                set.push(ValueRange::Single('-'));
                            }
                            None => set.push(ValueRange::Single('-')), // [-c]
                        },
                        Some((_, c)) => set.push(ValueRange::Single(c)),
                    }
                }

                if let Some(c) = range_lhs {
                    // [c-]
                    set.push(ValueRange::Single(c));
                    set.push(ValueRange::Single('-'));
                }

                elements.push(ElementBuilder::set(set.into()).one());
            }
            ']' => return Err(ParseError::UnexpectedGroupEnd(i)),

            '|' => elements.add_alteration(),

            _ => elements.push(ElementBuilder::char(c).one()),
        }
    }

    Ok(elements.take()?)
}

/// Utility class for creating elements.
#[derive(Debug, Clone)]
pub struct ElementBuilder(Value);

#[allow(dead_code)]
impl ElementBuilder {
    pub fn char(c: char) -> Self {
        Self(Value::One(c))
    }

    pub fn group(v: Vec<Element>) -> Self {
        Self(Value::Group(v))
    }

    pub fn wildcard() -> Self {
        Self(Value::Wildcard)
    }

    pub fn or(left: Element, right: Element) -> Self {
        Self(Value::Or(Box::new(left), Box::new(right)))
    }

    pub fn set(s: Vec<ValueRange>) -> Self {
        Self(Value::Set(HashSet::from_iter(s)))
    }

    pub fn one(self) -> Element {
        Element {
            value: self.0,
            quantifier: Quantifier::One,
        }
    }

    pub fn optional(self) -> Element {
        Element {
            value: self.0,
            quantifier: Quantifier::Optional,
        }
    }

    pub fn many(self) -> Element {
        Element {
            value: self.0,
            quantifier: Quantifier::Many,
        }
    }

    pub fn zero_or_many(self) -> Element {
        Element {
            value: self.0,
            quantifier: Quantifier::ZeroOrMany,
        }
    }

    pub fn range(self, min: u32, max: u32) -> Element {
        Element {
            value: self.0,
            quantifier: Quantifier::Range { min, max },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{parse_str, ElementBuilder as B, ParseError, ValueRange as R};
    #[test]
    fn chars() {
        assert_eq!(parse_str("x"), Ok(B::char('x').one()));
        assert_eq!(
            parse_str("ab"),
            Ok(B::group(vec![B::char('a').one(), B::char('b').one()]).one())
        );
        assert_eq!(
            parse_str("dy-"),
            Ok(B::group(vec![
                B::char('d').one(),
                B::char('y').one(),
                B::char('-').one()
            ])
            .one())
        );
        assert_eq!(
            parse_str(".."),
            Ok(B::group(vec![B::wildcard().one(), B::wildcard().one()]).one())
        );
        assert_eq!(
            parse_str("x🙃y"),
            Ok(B::group(vec![
                B::char('x').one(),
                B::char('🙃').one(),
                B::char('y').one()
            ])
            .one())
        );
    }

    #[test]
    fn quantifiers() {
        assert_eq!(
            parse_str("a*b"),
            Ok(B::group(vec![B::char('a').zero_or_many(), B::char('b').one()]).one())
        );
        assert_eq!(
            parse_str("cd+"),
            Ok(B::group(vec![B::char('c').one(), B::char('d').many()]).one())
        );
        assert_eq!(
            parse_str("a?c?"),
            Ok(B::group(vec![B::char('a').optional(), B::char('c').optional()]).one())
        );
        assert_eq!(
            parse_str("a.*b?"),
            Ok(B::group(vec![
                B::char('a').one(),
                B::wildcard().zero_or_many(),
                B::char('b').optional()
            ])
            .one())
        );
        assert_eq!(parse_str(".*"), Ok(B::wildcard().zero_or_many()));

        assert_eq!(parse_str("cd+?"), Err(ParseError::MultipleQuantifier(3)));
        assert_eq!(parse_str("*."), Err(ParseError::InitialQuantifier));

        assert_eq!(parse_str("a{3}"), Ok(B::char('a').range(3, 3)));
        assert_eq!(parse_str("b{2,5}"), Ok(B::char('b').range(2, 5)));
        assert_eq!(parse_str("c{7,7}"), Ok(B::char('c').range(7, 7)));

        assert!(matches!(
            parse_str("c{-1,1}"),
            Err(ParseError::IntegerParseError(_))
        ));
        assert_eq!(parse_str("{1,3}"), Err(ParseError::InitialQuantifier));
        assert_eq!(parse_str("a{4,3}"), Err(ParseError::InvertedRange(4, 3)));
        assert_eq!(parse_str("a 4,3}"), Err(ParseError::UnexpectedGroupEnd(5)));
        assert_eq!(parse_str("a{4,3"), Err(ParseError::UnfinishedGroup(1)));
        assert_eq!(
            parse_str("a{2}{2,3}"),
            Err(ParseError::MultipleQuantifier(4))
        );
    }

    #[test]
    fn groups() {
        assert_eq!(
            parse_str("(b)a"),
            Ok(B::group(vec![B::char('b').one(), B::char('a').one()]).one())
        );
        assert_eq!(
            parse_str("a(b.)+"),
            Ok(B::group(vec![
                B::char('a').one(),
                B::group(vec![B::char('b').one(), B::wildcard().one()]).many()
            ])
            .one())
        );
        assert_eq!(
            parse_str("c(ab*)*d?"),
            Ok(B::group(vec![
                B::char('c').one(),
                B::group(vec![B::char('a').one(), B::char('b').zero_or_many()]).zero_or_many(),
                B::char('d').optional()
            ])
            .one())
        );
        assert_eq!(
            parse_str("()d"),
            Ok(B::group(vec![B::group(vec![]).one(), B::char('d').one()]).one())
        );

        assert_eq!(parse_str("a(("), Err(ParseError::UnfinishedGroup(1)));
        assert_eq!(parse_str("ab*)("), Err(ParseError::UnexpectedGroupEnd(3)));
    }

    #[test]
    fn alteration() {
        assert_eq!(
            parse_str("a|bc"),
            Ok(B::or(
                B::char('a').one(),
                B::group(vec![B::char('b').one(), B::char('c').one()]).one()
            )
            .one())
        );
        // Couldn't find any information online on whether this is actually a valid regex.
        assert_eq!(
            parse_str("|b*"),
            Ok(B::or(B::group(vec![]).one(), B::char('b').zero_or_many()).one())
        );
        assert_eq!(
            parse_str("(a|b|c)+"),
            Ok(B::or(
                B::or(B::char('a').one(), B::char('b').one()).one(),
                B::char('c').one()
            )
            .many())
        );
        assert_eq!(
            parse_str("(x|y)*|z+"),
            Ok(B::or(
                B::or(B::char('x').one(), B::char('y').one()).zero_or_many(),
                B::char('z').many()
            )
            .one())
        );

        assert_eq!(parse_str("ab(cd|"), Err(ParseError::UnfinishedGroup(2)));
    }

    #[test]
    fn set() {
        assert_eq!(
            parse_str("[abc-de]"),
            Ok(B::set(vec![
                R::Single('a'),
                R::Single('b'),
                R::Range('c', 'd'),
                R::Single('e')
            ])
            .one())
        );
        assert_eq!(
            parse_str("[a-zA-Z]"),
            Ok(B::set(vec![R::Range('a', 'z'), R::Range('A', 'Z')]).one())
        );
        assert_eq!(
            parse_str("[a-b-c]"),
            Ok(B::set(vec![R::Range('a', 'b'), R::Single('-'), R::Single('c')]).one())
        );
        assert_eq!(
            parse_str("[a-]"),
            Ok(B::set(vec![R::Single('a'), R::Single('-')]).one())
        );
        assert_eq!(
            parse_str("[-a]"),
            Ok(B::set(vec![R::Single('-'), R::Single('a')]).one())
        );
        assert_eq!(
            parse_str("[a--]"),
            Err(ParseError::InvertedSetRange('a', '-'))
        );
        assert_eq!(
            parse_str("[!--]"),
            Ok(B::set(vec![R::Range('!', '-')]).one())
        );

        assert_eq!(parse_str("abc[123"), Err(ParseError::UnfinishedGroup(3)));
        assert_eq!(parse_str("123]"), Err(ParseError::UnexpectedGroupEnd(3)));
    }

    #[test]
    fn complex() {
        assert_eq!(
            parse_str("(a*(b|c)|(d|e+))+|(f|g?|h+)*"),
            Ok(B::or(
                B::or(
                    B::group(vec![
                        B::char('a').zero_or_many(),
                        B::or(B::char('b').one(), B::char('c').one()).one()
                    ])
                    .one(),
                    B::or(B::char('d').one(), B::char('e').many()).one()
                )
                .many(),
                B::or(
                    B::or(B::char('f').one(), B::char('g').optional()).one(),
                    B::char('h').many()
                )
                .zero_or_many()
            )
            .one())
        );

        assert_eq!(
            parse_str("[0-9a-f]+|[0-9A-F]+"),
            Ok(B::or(
                B::set(vec![R::Range('0', '9'), R::Range('a', 'f')]).many(),
                B::set(vec![R::Range('0', '9'), R::Range('A', 'F')]).many()
            )
            .one())
        );
    }
}
