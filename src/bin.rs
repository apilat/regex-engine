use regex_engine::{parse_str, Nfa};
use std::io::{prelude::*, stdin, BufReader};

fn main() {
    let mut regex: Option<Nfa> = None;

    let reader = BufReader::new(stdin());
    for line in reader.lines() {
        let line = line.expect("Failed to read line from stdin");
        if line.starts_with("compile ") {
            let line = &line[8..];
            let states = parse_str(line);
            println!("{:?}", states);
            if let Ok(states) = states {
                regex = Some(Nfa::new(&states));
                println!("{:?}", regex);
            }
        } else if let Some(regex) = &regex {
            println!("{}", regex.matches(&line));
        }
    }
}
